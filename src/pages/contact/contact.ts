import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController, AlertController } from 'ionic-angular';
import { ContactService } from './../../app/shared/services/contact.service';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  private form : FormGroup;
  private loading;
  public about;

  constructor(private formBuilder: FormBuilder, private alertCtrl: AlertController, public navCtrl: NavController, public loadingCtrl: LoadingController, public contactService: ContactService) {
    this.form = this.formBuilder.group({
      email: ['', Validators.email],
      subject: ['', Validators.required],
      text: ['', Validators.required]
    });
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Thanks',
      subTitle: 'Message was sent',
      buttons: ['Ok']
    });
    alert.present();
  }

  sendContact(){
    console.log(this.form.value);

    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    this.loading.present();

    this.contactService
      .create({
        email: this.form.controls['email'].value,
        subject: this.form.controls['subject'].value,
        description: this.form.controls['text'].value
      })
      .subscribe(json => {
          this.about = json.data[0];

          this.loading.dismiss();

          this.form.reset();

          this.presentAlert();
        }, error => {
          console.log(error);

          this.loading.dismiss();
      });
  }

}
