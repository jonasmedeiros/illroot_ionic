import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { AboutService } from './../../app/shared/services/about.service';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage implements OnInit {

  public about: Array<any> = [];

  constructor(public navCtrl: NavController, public aboutService: AboutService, platform: Platform) {
    platform.ready().then(() => {
      aboutService
        .getAll()
        .subscribe(json => {
            this.about = json.data[0];
          }, error => {
            console.log(error);
        });
    });
  }

  public ngOnInit():void {
    
  }

}
