import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { PostsService } from './../../app/shared/services/posts.service';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  public posts: Array<any> = [];  
  
  constructor(public navCtrl: NavController, public postsService: PostsService, platform: Platform, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      postsService
        .getAll()
        .subscribe(json => {
            splashScreen.hide();
            this.posts = json.data;
          }, error => {
            console.log(error);
        });
    });
  }

  public ngOnInit():void {
    
  }

  doRefresh(refresher) {

   this.postsService
      .getAll()
      .subscribe(json => {
          this.posts = json.data;
          refresher.complete();
        }, error => {
          console.log(error);
      });
  }

  public getImage(image) {

    if (!image) {
      return '';
    }

    let img = JSON.parse(image);
    return img[0].thumbnailUrl;
  }

}
