import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HttpClientModule } from '@angular/common/http';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// changed
import { PostsService } from './shared/services/posts.service';
import { AboutService } from './shared/services/about.service';
import { ContactService } from './shared/services/contact.service';
import { ItemComponent } from './shared/item/item.component';
import { IllAudioComponent } from './shared/ill-audio/ill-audio.component';

import { InAppBrowser } from '@ionic-native/in-app-browser';

import { ThemeableBrowser } from '@ionic-native/themeable-browser';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { Firebase } from '@ionic-native/firebase';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { FcmProvider } from '../providers/fcm/fcm';

const firebase = {
  apiKey: "AIzaSyCpux2CdnrOdO52EZphbZ40WWQFeXJRNwY",
  authDomain: "illroots-196814.firebaseapp.com",
  databaseURL: "https://illroots-196814.firebaseio.com",
  projectId: "illroots-196814",
  storageBucket: "illroots-196814.appspot.com",
  messagingSenderId: "307645350058"
 }

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ItemComponent,
    IllAudioComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    AngularFireModule.initializeApp(firebase),
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PostsService,
    AboutService,
    ContactService,
    InAppBrowser,
    ThemeableBrowser,
    GoogleAnalytics,
    Firebase,
    FcmProvider
  ]
})
export class AppModule {}
