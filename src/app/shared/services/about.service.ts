import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API, API_ROUTES } from './../../app.api.definitions';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators/map';

@Injectable()
export class AboutService {

  private dataResponse: any;

  constructor(public http: HttpClient) {}

  public getAll(): Observable<any> {

    let endpoint = `${API.domain}${API_ROUTES.about}`;

    return this.http
      .get(endpoint).pipe(
        map(json => this.extractData(json))
      );
  }

  private extractData(json) {
      this.dataResponse = json;
      return this.dataResponse;
  }

}
