import { Component, Input, OnInit } from '@angular/core';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'item',
  templateUrl: 'item.component.html'
})
export class ItemComponent implements OnInit {
  @Input() title: string;
  @Input() link: string;
  @Input() image: string;
  @Input() image64: string;

  constructor(private themeableBrowser: ThemeableBrowser, private iab: InAppBrowser, public _sanitizer: DomSanitizer) {}

  public ngOnInit() {}

  public goTo (url) {

    const options: ThemeableBrowserOptions = {
      toolbar: {
        height: 44,
        color: '#f0f0f0ff'
      },
      title: {
        color: '#000000',
        showPageTitle: true,
        staticText: 'ILLROOTS'
      },
      closeButton: {
        wwwImage: 'assets/close.png',
        align: 'left',
        event: 'closePressed'
      }
    };

    //if (url.indexOf('youtu') !== -1 || url.indexOf('youtube') !== -1) {

    //   let id = url.replace('https://youtu.be/', '').replace('https://www.youtube.com/watch?v=', '');

    //   this.youtube.openVideo('kLXgUK8WJM8');
    // } else {
      const browser: ThemeableBrowserObject = this.themeableBrowser.create(url, '_blank', options);

      browser.on('closePressed').subscribe(data => {
        browser.close();
      });
    // }
    
  }

  public goToNormal (url) {

    const options: InAppBrowserOptions = {
      zoom: 'no',
      closebuttoncaption: 'back',
      location: 'yes',
      toolbarposition: 'top'
    }

    this.iab.create(url, '_blank', options);
  }

  public fixImage(image) {
    return image.replace('original', 'square').replace('//', 'http://');
  }

  public makeTrustedImage(item) {

    let temp = 'data:image/jpeg;base64,' + item;

    const imageString =  JSON.stringify(temp).replace(/\\n/g, '');
    const style = 'url(' + imageString + ')';
    return this._sanitizer.bypassSecurityTrustStyle(style);
  }
}
