import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ill-audio',
  templateUrl: 'ill-audio.component.html'
})
export class IllAudioComponent implements OnInit {
  @Input() mp3: string;
  public audio;
  public playing: boolean = false;

  constructor() {}

  public ngOnInit() {}

  public play() {
    this.audio = new Audio(this.mp3);
    this.audio.play();
    this.playing = true;
  }

  public pause() {
    this.audio.pause();
    this.playing = false;
  }
}
