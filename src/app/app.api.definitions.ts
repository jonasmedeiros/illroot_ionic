export const API = {
  domain: 'https://illroot-api.herokuapp.com/api'
};

export const API_ROUTES = {
  posts: '/v1/posts',
  about: '/v1/about',
  contact: '/v1/contact'
};